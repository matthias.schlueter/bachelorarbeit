import umapping
import pandas as pd
import numpy as np
import sys





def classicDf():
    df = pd.read_csv("fimo_hits_chr4_forward.csv")
    fimo_hits = df
    max = fimo_hits["signal_value"].max()
    # euclidean_distance = sys.argv[3]
    for j in range(5):
        start = int(fimo_hits[fimo_hits["signal_value"] >= max]["start"].iloc[0])
        stop = int(fimo_hits[fimo_hits["signal_value"] >= max]["stop"].iloc[0])
        fimo_hits = fimo_hits.drop(fimo_hits[fimo_hits["signal_value"] >= max].index)
        max = fimo_hits["signal_value"].max()
        dna_dict = umapping.makeDNADict("TAIR10_chr_all.fas")
        chr4 = dna_dict["4"]
        print(start)
        n = 2
        sequence = []
        for i in range(stop - start + (n * 2) + 1):
            # print(i)
            sequence.append(chr4[start - 1 - n + i])
        sequence = ''.join(sequence)
        blast = []
        k= 7
        for i in range(stop - start + (k * 2) + 1):
            blast.append(chr4[start - 1 - n + i])
        blast = ''.join(blast)
        print(blast)
        for euclidean_distance in range(1):
            df = umapping.search(dna_dict, sequence, stop - start + 1, euclidean_distance + 1)
            df = df[df["genome"] == "4"]
            df.to_csv("/homes/mschluet/PycharmProjects/Projektmodul/CBF4_matches_" + str(euclidean_distance + 1) + "_" +
                      str(n) + "_" + str(j) + ".csv")
            print(np.shape(df))


if __name__ == '__main__':
    print("Dataframe: ")
    classicDf()

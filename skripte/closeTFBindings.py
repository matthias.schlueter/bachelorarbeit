import pandas as pd
import translate
import bachelor1
from math import dist
from itertools import  product
from tqdm import tqdm
import pickle
import matplotlib.pyplot as plt
import sys

if __name__ == '__main__':
    sequence = str(sys.argv[1])
    print(sequence)
    tmp = product('ACGT', repeat=2)
    pre = list((''.join(word) for word in tmp))
    post = pre
    #'{}_{:05}_{}_{:.5f}'.format(s1, i, s2, f)
    #["{}{:02}".format(b_, a_) for a_, b_ in zip(a, b)]
    output = ["{:2}{}{:2}".format(i, sequence, j) for i, j in zip(pre, post)]
    df = pd.DataFrame(list(output), columns=["Words"])
    print(df)
    distanceTable = bachelor1.getDistanceTable(df)
    #output = ["".join([sequence, str(pre)]) for pre in words]
    print(distanceTable)


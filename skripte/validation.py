import sys
import pandas as pd
import numpy as np

if __name__ == '__main__':
    file_experiment = pd.read_csv("fimo_hits_chr4_forward.csv")
    statistics = []
    experiment = file_experiment
    experiment_valid = experiment[experiment["signal_value"] > 0]
    for j in range(5):
        for i in range(6):
            file_program = pd.read_csv("matches_2/matches_" + str(i+1) + "_2_" + str(j) + ".csv")
            print(i)

            program = file_program
            print(file_program)
            program_reduced = program[program["confidence"]<6]
            print(program_reduced)
            number_of_matches = np.shape(program_reduced)[0]
            print(number_of_matches)
            number_in_experiment = np.shape(pd.DataFrame(program_reduced["start"]+3).merge(pd.DataFrame(experiment["start"])))[0]
            print(number_in_experiment)
            number_with_value = np.shape(experiment_valid)[0]
            print(number_with_value)
            valids_found_by_program = np.shape(pd.DataFrame(program_reduced["start"] + 3).merge(pd.DataFrame(experiment_valid["start"])))[0]
            match_rate = float(valids_found_by_program)/float(number_of_matches)*100
            miss_match_rate = float(number_in_experiment-valids_found_by_program)/number_of_matches*100
            valid_found_percentage = float(valids_found_by_program)/float(number_with_value)*100
            statistics.append([i+1, number_of_matches, number_in_experiment, number_with_value, valids_found_by_program, match_rate, miss_match_rate, valid_found_percentage])
        df =pd.DataFrame(statistics, columns=["distance", "programm", "examined", "valids", "program val", "Matchrate", "false found", "valid rate"] )
        df.to_csv("CBF4_validation_2_" +str(j) +"6.csv")
    #print(experiment["start"])
import numpy as np
import csv
import decimal
import pandas as pd


def getneighbourhood(data, positions, i):
    """
    This function extracts the position of the line i in the genome.

    :param data: The genome
    :param positions: The positions in the genome to extract the minimum and maximum of the attributes from.
    :param i: The row inside a binding site consisting of more then one line.
    :return: The dataframe with the i.th line of all binding sites.
    """
    shortN = []
    for row in positions:
        shortN.append(data.loc[row+i+1])
    shortN_df = pd.DataFrame(shortN)
    return shortN_df



def getMinMaxNeighbourhood(data, positions, motif_length, string):
    """
    This method has the purpose to get the absolute mimimum and maximum of all shape-values of all base positions in the
    average motif. The maximum and minimum is extracted from the chromosom 4 shape examining all the positions which are
    binding sites for the cbf4 TF.

    :param data: This is the genome-shape information.
    :param positions: These are the binding sites positions.
    :param motif_length: The length of the average motif without empty positions.
    :param string: This is just part of the data name.

    :return: Is the minimum and maximum of each value and line in the average motif.
    """
    positions = positions - 7

    data = data.drop(axis=1, labels="Unnamed: 0")
    keys = data.keys()

    index = range(2)
    minMax = pd.DataFrame(np.nan, index=index, columns=keys)

    index = range(motif_length*2)
    newdata = pd.DataFrame(np.nan, index=index, columns=keys)
    """
    Builds row by row the conditions dataframe. Extracting the minimum and the maximum of the values at the i.th row
    of the binding sites.
    """
    for i in range(motif_length):
        motif_part = getneighbourhood(data, positions, i)
        minimum = pd.DataFrame.min(motif_part)
        maximum = pd.DataFrame.max(motif_part)
        minMax.loc[0] = minimum
        minMax.loc[1] = maximum
        newdata.loc[i*2] = minimum
        newdata.loc[1+(i*2)] = maximum

    newkeys = []
    """
    The following part reconstructs the extracted data to a form which is easier to work with in the other scripts.
    Instead of saving min and max in two lines, the programms connects them together.

    Former:
                Stagger, Bucket, ...
    pos1: min
    pos1: max

    After:

     pos1: min_stagger, max_stagger, min_bucket, max_bucket,
    """
    for i in range(len(keys)):
        minkey = "min_" + keys[i]
        maxkey = "max_" + keys[i]
        newkeys.append(minkey)
        newkeys.append(maxkey)

    index=range(motif_length)

    newCSV = pd.DataFrame(np.nan, index=index, columns=newkeys)
    newCSV_list = []
    for i in range(newdata.shape[0]//2):
        newCSV_line = []

        for key in keys:
            newCSV_line.append(newdata.loc[i*2][key] - 0.0000001)
            newCSV_line.append(newdata.loc[1+i*2][key] + 0.00000001)

        newCSV_list.append(newCSV_line)

    newCSV = pd.DataFrame(newCSV_list, columns=newkeys)
    """
    This is the extracted data written to a file to use it in the other script.
    """
    newCSV.to_csv("/homes/mschluet/Dokumente/Projektmodul/HY5_" + string + "minAndmax.csv")

    return newdata





def minAndMax(data):
    data = data.drop(axis=1, labels="Unnamed: 0")
    keys=data.keys()
    index=range(2)
    newdata = pd.DataFrame(np.nan, index=index, columns=keys)
    minimum = pd.DataFrame.min(data)
    maximum = pd.DataFrame.max(data)
    newdata.loc[0]=minimum
    newdata.loc[1]=maximum
    return newdata

#def foundByFimo(positions, chr):
#    fimo = []
 #   for row in positions:
  #      fimo.append(chr.T[row+1])
   # df=pd.DataFrame(fimo)
    #df.to_csv("/homes/mschluet/Dokumente/Projektmodul/try", mode="a")
    #return df


if __name__ == '__main__':
    """
    This skript is supposed to contstruct a dataset using the fimo_hits datas from the experiment.
    This dataset is later used in the other scripts (project and main) to assess the binding of TFs in a genome with
    the genome data, this dataset, and the avarage motif of the TF.


    chr4_df = The genome shape dataframe.

    average_df = The average motif of the TF

    fimo_hits_df = The values from the experiment for the cbf4 TF on chr4.

    """
    df = pd.read_csv("/prj/fimo-plus/data/chr4_shape.csv")
    chr4_df = df
    df = pd.read_csv("/prj/fimo-plus/data/HY5/fimo_hits_chr4_forward.csv")
    fimo_hits_df = df

    df = pd.read_csv("/prj/fimo-plus/data/HY5/hy5_average_motif_shape.csv")
    average_df = df.dropna()
    motif_length = average_df.shape[0]

    count = 0
    filler =True
    """
    This loop makes it possible to cut the the data into smaller pieces. This was implemented for the functions which are
    not used right know but could be usefull later in the project.

    """
    while filler:
        old = count
        count = count + chr4_df.shape[0]
        filler = False
        chr4_short = chr4_df[old:count]
        small = fimo_hits_df[(fimo_hits_df["start"]<count) & (fimo_hits_df["start"]>old)]
        val_positions_df = small[small["signal_value"]!=0.0]

        positions = small["start"]
        val_positions = val_positions_df["start"]
        val_positions_list = val_positions.tolist()
        #positions_list = positions.tolist()
        #val_fimo =foundByFimo(val_positions, chr4_df)
        #fimo = foundByFimo(positions_list, chr4_df)
        #print(count)
        #minAndMax_val = minAndMax(val_fimo)
        #minAndMax = minAndMax(fimo)

    val_minMaxNeighbourhood  = getMinMaxNeighbourhood(chr4_short, val_positions, motif_length, "try_val_")
    minAndMaxNeighbourhood = getMinMaxNeighbourhood(chr4_short, positions, motif_length, "try_")

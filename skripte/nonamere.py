import pickle
import translate
from math import dist
import sys
import numpy as np
import pandas as pd

"""
this function adds to an kmer one of the Letters A, C, G, T. So ACGT would develope to ACGTA, ACGTC, ACGTG and ACGTT.
"""
def makeMore(suffixe):
  alphabet = ["A", "C", "G", "T"]
  all = []
  for suffix in suffixe:
    pentas = []
    for letter in alphabet:
      penta = (suffix[0], suffix[1] + letter)
      pentas.append(penta)
    all.append(pentas)
  array = np.reshape(all, (len(all) * 4, 2 ))
  return(array)

"""
This function computes the distance of two different shapes (does only work for shapes of the same lenght)
"""
def distanceOfShapesPerBase(shapeOne, shapeTwo):
  distanceList = []
  for index, row in shapeOne.iterrows():
    distanceList.append(dist(row, shapeTwo.loc[(index)]))
  return distanceList

"""
This function builds a string from the neighbours of the pentamere in the original sequence
"""
def buildString(possible_neighbours, next_neighbours):
  tmps = []
  for possible_neighbour in possible_neighbours:
    if(possible_neighbour[1] in next_neighbours):
      tmp = possible_neighbour[0] + possible_neighbour[1]
      tmps.append(tmp)
  return tmps

"""
This function constructs a neighborhood.
length: choses the database
sequence: is the given sequence which neighborhood will be constructed
euclidean distance: defines the distance a neighbour of each part-sequence is allowed to have.
"""
def buildNeighbourhood(length, sequence, euclidean_distance):
  path_better = str(length) + "_mere_distances.csv"
  df_neighbour = pd.read_csv(path_better)
  tmp_neighbours = df_neighbour["Unnamed: 0"][df_neighbour[sequence[0:5]]<=euclidean_distance]
  prefixe = tmp_neighbours.str.slice(stop=length - 1)
  suffixe = tmp_neighbours.str.slice(start=1)
  start = tmp_neighbours.str.slice(stop=1)
  stop = tmp_neighbours.str.slice(start=-1)
  tmp_neighbours = pd.concat([tmp_neighbours.rename("neighbour"), prefixe.rename("prefix"), suffixe.rename("suffix"), start.rename("start"), stop.rename("stop")], axis=1 )
  for i in range(len(sequence) - 5):
    pentamere = sequence[i + 1:i + 6]
    tmp_neighbours_next=df_neighbour["Unnamed: 0"][df_neighbour[pentamere] <= euclidean_distance]
    prefixe = tmp_neighbours_next.str.slice(stop=length - 1)
    suffixe = tmp_neighbours_next.str.slice(start=1)
    start = tmp_neighbours_next.str.slice(stop=1)
    stop = tmp_neighbours_next.str.slice(start=-1)
    tmp_neighbours_next = pd.concat([tmp_neighbours_next.rename("neighbour"), prefixe.rename("prefix"), suffixe.rename("suffix"), start.rename("start"), stop.rename("stop")], axis=1)
    tmp_neighbours = tmp_neighbours_next.merge(tmp_neighbours, left_on=['prefix'], right_on=['suffix'], how='left').dropna()
    tmp_neighbours["start"] = tmp_neighbours["start_y"] + tmp_neighbours["start_x"]
    tmp_neighbours = tmp_neighbours.drop(axis = 1,  columns=["start_y", "start_x", "stop_y", "prefix_y", "suffix_y", "neighbour_y"])
    tmp_neighbours = tmp_neighbours.rename(columns={"neighbour_x":"neighbour", "stop_x":"stop", "prefix_x":"prefix", "suffix_x":"suffix"})
    #tmp_neighbours.rename( )
  #print(tmp_neighbours)
  tmp_neighbours["end_neighbour"] = tmp_neighbours["start"] + tmp_neighbours["suffix"]
  reference = translate.seq_to_shape(sequence).dropna().to_numpy().flatten()
  confidence = tmp_neighbours["end_neighbour"].apply(distance, args=(reference,))
  tmp_neighbours["distance"] = confidence
  tmp_neighbours =tmp_neighbours.drop(axis=1, columns=["neighbour", "prefix", "suffix", "start", "stop"])
  return  tmp_neighbours

def distance(sequence, reference):
  return dist(reference, translate.seq_to_shape(sequence).dropna().to_numpy().flatten())




def nonamere(sequence, euclidean_distance):
  dict_pent_neighbours = pickle.load( open( "pent_neighbour_pickles/pent_neighbours_" + str(euclidean_distance) + ".p", "rb" ) )
  print(sequence)
  example =  sequence
  example_neighbours = []
  tmp_neighbours = []
  #possible_neighbours = set()
  for i in range(len(example)-4):
    if not tmp_neighbours:
      tmp_neighbours = dict_pent_neighbours[example[i:i+5]]
    else:
      tmp_next_neighbours = dict_pent_neighbours[example[i:i+5]]
      if i == 1:
        präfix_suffix_tmp = {(k[0], k[1:5]) for k in tmp_neighbours}
      else:
        präfix_suffix_tmp = {(k[0:i], k[i:(i+5)]) for k in tmp_neighbours}
      possible_neighbours = makeMore(präfix_suffix_tmp)
      tmp_neighbours = buildString(possible_neighbours, tmp_next_neighbours)
  reference = translate.seq_to_shape(sequence).dropna().to_numpy().flatten()
  tuple_list = []
  for neighbour in tmp_neighbours:

    tuple_list.append((neighbour, dist(reference, translate.seq_to_shape(neighbour).dropna().to_numpy().flatten())))

  """
  Distances: to show in the bachelorwork
    for neighbour in tmp_neighbours:
    shape = translate.seq_to_shape(neighbour)[2:-2]
    shapeOriginal = translate.seq_to_shape(example)[2:-2]
    distanceOfShapesPerBase(shapeOriginal, shape)
  """
  print(tuple_list)
  return tuple_list





  

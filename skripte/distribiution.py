import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def filter(df_neighbour, eu_dist):
    distribution = []
    keys = df_neighbour.keys()
    for key in keys:
        distribution.append(df_neighbour[key][df_neighbour[key]< eu_dist].shape[0])
    minimum = np.min(distribution)
    maximum = np.max(distribution)
    average = sum(distribution)/np.shape(distribution)[0]
    return minimum, maximum, average

if __name__ == '__main__':
    length = [5, 6, 7]
    for x in length:
        path = str(x) + "_mere_distances.csv"
        df_neighbour = pd.read_csv(path)

        df_neighbour = df_neighbour.set_index(df_neighbour["Unnamed: 0"]).drop(columns=["Unnamed: 0"])
        minima = []
        maxima = []
        averages = []
        distances = range(1,11)
        for eu_dist in range(1, 11):
            min, max, average = filter(df_neighbour, eu_dist)
            minima.append(min)
            maxima.append(max)
            averages.append(average)
        fig = plt.figure()
        plt.plot(distances, minima, label="minima")
        plt.plot(distances, maxima, label="maxima")
        plt.plot(distances, averages, label="average")
        plt.legend(loc="upper left")
        plt.xlabel("Euclidean distances")
        plt.ylabel("Neighborhood size of " + str(x) + "-mer")
        plt.show()
        fig.savefig("Neighborhood size of " + str(x) + "-mer.png", dpi=fig.dpi)
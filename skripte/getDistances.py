import numpy as np
import csv
import pandas as pd

df = pd.read_csv("/prj/fimo-plus/data/HY5/hy5_average_motif_shape.csv")
average_df = df[2:-2]
average_len = average_df.shape[0] 

df = pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/HY5_position_frames")
positions = df.drop("Unnamed: 0", axis=1)

df = pd.read_csv("/prj/fimo-plus/data/chr4_shape.csv")
chr4 = df.dropna()

datapoints  = chr4[chr4.index.isin(positions["Row"]+1)]
datapoints.to_csv("/homes/mschluet/Dokumente/Projektmodul/HY5_chr4_matches.csv")

i=0
for row in datapoints:
    diff = row[:1] - average_df.iloc[i]


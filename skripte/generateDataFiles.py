import bachelor1 as b1
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

if __name__ == '__main__':
    figs = {}
    for i in range(10):
        titel =  "fig_" + str(i)
        distribution, most = b1.main(i +1)
        plt.figure(i+1)
        plt.plot(distribution[0:most])
        dataName = titel = "number_pent_neighbours_" + str(i+1) + ".png"
        plt.savefig(dataName)

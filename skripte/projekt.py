import numpy as np
import csv
import decimal
import pandas as pd




if __name__ == '__main__':
    """
    This class deletes the "Unnamed: 0" column and adds the conservation to the minAndMax data.

    average_df = The average motif of the TF

    minAndMax = The conditions are extracted in skript data and modified in project.
    """
    df = pd.read_csv("/prj/fimo-plus/data/HY5/hy5_average_motif_shape.csv")
    average_df = df
    df = pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/HY5_try_val_minAndmax.csv")
    minAndMax = df
    average_df = average_df.dropna()
    s = pd.Series(range(average_df.shape[0]))
    average_df.set_index(s, inplace=True)



    minAndMax["conservation"]=(average_df["conservation"])
    minAndMax = minAndMax.set_index(["conservation"])
    minAndMax = minAndMax.drop(labels="Unnamed: 0", axis=1)
    print(minAndMax)
    minAndMax.to_csv("/homes/mschluet/Dokumente/Projektmodul/HY5_try_val_cons_minAndmax.csv")

    minAndMax_cons = pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/HY5_try_val_cons_minAndmax.csv")
    index = []

    for i in range(average_df.shape[0]):
        possible = minAndMax_cons[minAndMax_cons["conservation"]-(average_df.iloc[i]["conservation"] - 0.0000001)>0]
        index.append(possible[possible["conservation"] == min(possible["conservation"])].index.values[0])

    print(index)




    #filter_list = []
    #filter_list.append()
    #print(minAndMax)


import pandas as pd

if __name__ == '__main__':
    df = pd.read_csv("//homes/mschluet/Downloads/1-Alignment-HitTable.csv")
    df.columns = ['new_col1', 'new_col2', 'new_col3', 'new_col4', 'new_col5', 'new_col6', 'new_col7', 'new_col8', 'new_col9', 'new_col10', 'new_col11','new_col12']
    blast = df

    df = pd.read_csv("fimo_hits_chr4_forward.csv")
    fimo_hits_HY5 = df
    count = 0
    count_val = 0
    for i in range(40):

        counter_val = blast[(blast["new_col9"]-20 + i).isin(fimo_hits_HY5["start"][fimo_hits_HY5["signal_value"]> 0.1])]
        counter = blast[(blast["new_col9"]-20 + i).isin(fimo_hits_HY5["start"])]
        print(counter.shape)
        count =count + int(counter.shape[0])
        count_val = count_val +int(counter_val.shape[0])
        print(count)
        print(count_val)
"""
35, 17 ACACGTGGCGTTACCTGATG 195
47, 12 TGACGTGGCGGATTGTGATT 326
59, 26 TGACGTGGCTACCTATTTGG 166
36, 20 ACACGTGGCTACGAGTCAAG 142
46, 20 TGACGTGGCGATAATGGATT 267

ATGTCGGCAAGGGAAGACTT&259&17&14
ATGTCGGCTGACCGACGATG&80&39&24
ATGTCGGTCGGTCTCTTGTT&220& 38& 24
ATGTCGGTCGCTTTGTGAAG& 241& 22& 12
ATGTCGGCAGACTTCTTTGA& 367&9&7
"""
import numpy as np
import csv
import decimal
import pandas as pd



def filter(minAndMax, chr_df, average):
    """
    This is the filter method to get all the valid start positions of the possible binding sites.

    :param minAndMax: Those are the intervalls/conditions.
    :param chr_df: Is the genome shape data.
    :param average: Is the average motif of the TF.
    :return: A file with the valid start positions of the binding sites
    """
    print(0)
    index = []

    """
    This compares the data extracted in the "data" script with the given average motif of the TF. It looks for the next
    bigger conservation score and saves for this line the row of the conditions in the minAndMax array which are
    supposed to be used.
    """
    for i in range(average_df.shape[0]):
        possible = minAndMax[minAndMax["conservation"] - (average.iloc[i]["conservation"] - 0.0000001) > 0]
        index.append(possible[possible["conservation"] == min(possible["conservation"])].index.values[0])

    data = positionI(minAndMax, chr_df, 0).dropna().astype(int)

    """
    This searches for the lines, which met the conditions and reduces the dataframe to those who met all conditions so
    far.
    """
    #fail = []
    print(data)
    for i in index:
        #fail.append(positionI(minAndMax, chr_df, i))
        data = data[data.isin(positionI(minAndMax, chr_df, i).dropna().astype(int))].dropna()
        data = data +1
        #print(fail)
        print(data)
    data = data - average_df.shape[0] + 1
    data_df = data.to_frame()
    s = pd.Series(range(data.shape[0]))
    data_df.set_index(s, inplace=True)
    print(data_df)
    data_df.to_csv("/homes/mschluet/Dokumente/Projektmodul/HY5_search")


def positionI(minAndMax, chr_df, i):
    """
    This method tests for line i in the condition dataframe and returns the positions of the rows of the genome which
    met the conditions.

    :param minAndMax: The conditions.
    :param chr_df: The genome.
    :param i: The row in the condition dataframe
    :return: rows of the genome which met the conditions.
    """
    data = chr_df
    chr_keys = chr_df.keys()
    minmax_keys = minAndMax.keys()
    for j in range(1,len(chr_keys)):
        """
        The filter positions has to be saved cause of the different structure of the condition dataframe.
        The condition dataframe has for every attribute in the genome dataframe two attributes. The min and the max of
        the value.
        """
        min_filter_pos = j * 2 -1
        max_filter_pos = j*2
        min_filter = minAndMax.loc[i][minmax_keys[min_filter_pos]]
        max_filter = minAndMax.loc[i][minmax_keys[max_filter_pos]]
        data=data[(data[chr_keys[j]]>=min_filter) & (data[chr_keys[j]]<=max_filter)]
    position = data["Unnamed: 0"]
    position.tolist()
    print("positionI.2")
    return position


if __name__ == '__main__':
    """
    This script is returning all the positions that meet the conditions. This conditions are an special intervalls for
    each line where all their values have to be in the special intervall for their column. A line is valid, if all
    columns met their conditions. A position is valid, if all lines met their conditions.

    chr4_df = The genome shape dataframe.

    average_df = The average motif of the TF

    minAndMax = The conditions extracted in data and modified in project.

    The script is splitted in different parts for runtime purposes.

    """
    df=pd.read_csv("/prj/fimo-plus/data/chr4_shape.csv")
    chr4_df= df
    df = pd.read_csv("/prj/fimo-plus/data/HY5/hy5_average_motif_shape.csv")
    average_df =df
    df=pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/HY5_try_val_cons_minAndmax.csv")
    minAndMax = df
    average_df = average_df.dropna()
    s = pd.Series(range(average_df.shape[0]))
    average_df.set_index(s, inplace=True)
    filter(minAndMax, chr4_df, average_df)

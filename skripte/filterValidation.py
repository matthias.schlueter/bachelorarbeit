import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    df = pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/HY5_search")
    positions_HY5 = df.drop("Unnamed: 0", axis=1)
    df = pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/search")
    positions_CBF4 = df.drop("Unnamed: 0", axis=1)
    df = pd.read_csv("/prj/fimo-plus/data/HY5/fimo_hits_chr4_forward.csv")
    fimo_hits_HY5 = df
    df = pd.read_csv("/prj/fimo-plus/data/fimo_hits_chr4_forward.csv")
    fimo_hits_CBF4 = df
    number_cbf4 = positions_CBF4.shape[0]
    number_hy5 = positions_HY5.shape[0]
    print("CBF4 FIMO: " + str(fimo_hits_CBF4.shape[0]))
    print("HY5 FIMO: " + str(fimo_hits_HY5.shape[0]))
    fimo_hit_len_HY5 =  fimo_hits_HY5.shape[0]
    fimo_hit_len_CBF4 = fimo_hits_CBF4.shape[0]
    experiments_CBF4 = fimo_hits_CBF4["start"][fimo_hits_CBF4["signal_value"] > 0.01].shape[0]
    experiments_HY5 = fimo_hits_HY5["start"][fimo_hits_HY5["signal_value"] > 0.01].shape[0]
    real_positions_HY5 = positions_HY5["Unnamed: 0.1"] + 6
    real_positions_CBF4 = positions_CBF4["Row"] + 6

    hits_HY5 = real_positions_HY5[real_positions_HY5.isin(fimo_hits_HY5["start"])].shape[0]
    hits_CBF4 = real_positions_CBF4[real_positions_CBF4.isin(fimo_hits_CBF4["start"])].shape[0]
    fimo_HY5 = hits_HY5 / fimo_hit_len_HY5
    fimo_cbf4 = hits_CBF4 / number_cbf4
    number_true_hits_hy5 = real_positions_HY5[real_positions_HY5.isin(fimo_hits_HY5["start"][fimo_hits_HY5["signal_value"] > 0.01])].shape[0]
    number_true_hits_cbf4 = real_positions_CBF4[real_positions_CBF4.isin(fimo_hits_CBF4["start"][fimo_hits_CBF4["signal_value"] > 0.01])].shape[0]
    true_hits_HY5= real_positions_HY5[real_positions_HY5.isin(fimo_hits_HY5["start"][fimo_hits_HY5["signal_value"] > 0.1])].shape[0] / experiments_HY5
    names = ["fimo", "hits", "failures"]
    values = [fimo_HY5, true_hits_HY5, ((positions_HY5.shape[0] - hits_HY5) / positions_HY5.shape[0])]
    number_program = [number_hy5, number_cbf4]
    fimo_hits = [fimo_hit_len_HY5, fimo_hit_len_CBF4]
    experiments = [experiments_HY5, experiments_CBF4]
    hits = [hits_HY5, hits_CBF4]
    number_true_hits = [number_true_hits_hy5, number_true_hits_cbf4]
    number_false_hits = [(number_hy5 - hits_HY5), (number_cbf4 - hits_CBF4)]
    columns = ["found by filter", "found by fimo", "validated by ampDAP-seq", "match fimo and filter", "match of fimo and ampDAP-seq", "missmatches"]
    result_df = pd.DataFrame(list(zip(number_program, fimo_hits, experiments, hits, number_true_hits, number_false_hits)), columns = columns)
    print(result_df)
    result_df.to_csv("/homes/mschluet/Dokumente/Projektmodul/filterEndResults")
    print(list(zip(number_program, fimo_hits)))

    plt.bar(names, values)
    plt.ylabel("percentage")
    plt.savefig("HY5_results_filter")
    plt.show()

    df = pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/HY5_try_val_cons_minAndmax.csv")
    HY5_minAndMax = df
    df = pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/try_val_cons_minAndmax.csv")
    minAndMax = df
    print(HY5_minAndMax.keys())
    keys = HY5_minAndMax.drop(axis=1, columns=['conservation']).keys()
    print(keys)
    print(HY5_minAndMax)
    print(minAndMax)
    i=0
    plt.ylabel("shape value")
    minAndMax.sort_values(by='conservation').plot(x="conservation", y=["min_Stagger", "max_Stagger"], kind = "line")
    plt.savefig("CBF4_Stagger_conservation")
    minAndMax.sort_values(by='conservation').plot(x="conservation", y=["min_Buckle", "max_Buckle"], kind = "line")
    plt.savefig("CBF4_Buckle_conservation")
    plt.show()
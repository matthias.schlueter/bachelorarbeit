import pandas as pd
import translate
from math import dist
from itertools import  product
import pickle
import os


def getDistance(shapeOne, shapeTwo):
  arrayOne = shapeOne.to_numpy().flatten()
  arrayTwo = shapeTwo.to_numpy().flatten()
  return dist(arrayOne, arrayTwo)


"""
This function computes the shapes of the kmers of lenght five like AAAAA or ACGTA and calculates the euclidean distance between all of them
"""
def getDistanceTable(df, length):
  shapes_dict = {}
  shapes = pd.DataFrame
  for index, row in df.iterrows():
    if(index == 0):
      shapes_dict[row["Words"]] = translate.seq_to_shape(row["Words"])[2:-2]
      shapes = translate.seq_to_shape(row["Words"])[2:-2]
    else:
      shapes_dict[row["Words"]] = translate.seq_to_shape(row["Words"])[2:-2]
      shapes = pd.concat([shapes, translate.seq_to_shape(row["Words"])[2:-2]], ignore_index=True)
  distanceTable = []
  distanceTable_df = pd.DataFrame(index=df["Words"].tolist(), columns=df["Words"].tolist())
  for keyOne in shapes_dict.keys():
    for keyTwo in shapes_dict.keys():
      distanceTable_df.at[keyOne, keyTwo] = getDistance(shapes_dict[keyOne], shapes_dict[keyTwo])
  #for index, row in shapes.iterrows():
  #  distanceList = []
   # for indextwo, rowtwo in shapes.iterrows():
    #  distanceList.append(dist(row, rowtwo))
    #distanceTable.append(distanceList)
  #distanceTableDf = pd.DataFrame(distanceTable, columns=list(df["Words"]), index=list(df["Words"]))
  print(distanceTable_df)
  adress = str(length) + "_mere_distances.csv"
  distanceTable_df.to_csv(adress)
  return distanceTable_df


def buildComplexNeighbourhood(pentamere, similar_df, euclidean_distance, length):
  similar_list = list(similar_df.index)
  similar_properties_list = []
  path = str(length) + "_euclidean_distance_"  + str(euclidean_distance)
  if not (os.path.isdir(path)):
    os.mkdir(path)
  for similar in similar_list:
    similar_properties_list.append([similar, similar[0], similar[-1], similar[0:-1], similar[1:]])
  similar_properties_df = pd.DataFrame(similar_properties_list, columns=["neighbour", "start", "stop", "prefix", "suffix"])
  adress = path + "/" + str(pentamere) + ".csv"
  similar_properties_df.to_csv(adress)




"""
This script tries to find similar sequences in shape via kmers of length n.
"""
def main(euclidean_distance, length):
  tmpWords = product('ACGT', repeat=length)
  words = (''.join(word) for word in tmpWords)
  df = pd.DataFrame(list(words), columns = ["Words"])
  distanceTable = getDistanceTable(df, length)
  similars = {}
  #print(distanceTable)
  """keys= distanceTable.keys()
  most = 0
  least = pow(4, length)
  distribution = [0]*least
  dict_pent_neighbours = {}
  #print(keys)
  for key in keys:
    similar = distanceTable[distanceTable[key]< euclidean_distance]
    buildComplexNeighbourhood(key, similar, euclidean_distance, length)
    #print(zip(similar.index, similar[key]))
    dict_pent_neighbours[key] = set(similar.index)
    distribution[similar.shape[0]] = distribution[similar.shape[0]] + 1
    if(most<similar.shape[0]):
      most=similar.shape[0]
    if(similar.shape[0]< least):
      least = similar.shape[0]
  adress = str(length) + "_neighbours_" + str(euclidean_distance) + ".p"
  print(2)
  pickle.dump(dict_pent_neighbours, open( adress, "wb" ) )"""

  #for key, value in dict_pent_neighbours.items():
    

  #if value == desired_value:

   # del a_dictionary[key]

  #return (distribution, most)


    #print(len(words))
    #distanceRow = []
    #for secondWord in words:
     # secondWordShape = translate.seq_to_shape(secondWord)[2:-2].to_numpy()

      #for i in firstWordShape.size:
      #  dist(baseShape)


  #df = pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/average_motif_shape_cbf4.csv")
  #average_df = df[2:-2]
  #average_len = average_df.shape[0]
  #df = pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/fimo_hits_chr4_forward.csv")
  #experiment = df
  #df = pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/position_frames.csv")
  #positions = df.drop("Unnamed: 0")
  #print(positions)




import nonamere
import subprocess
import sys
from Bio import SeqIO
from Bio.Seq import Seq
import pandas as pd
import numpy as np

def makeDNADict(input_file):
    fasta_sequences = SeqIO.parse(open(input_file), 'fasta')
    dnas = {}
    for seq_record in fasta_sequences:
        str_seq = str(seq_record.seq)
        dnas[str(seq_record.id)] = str_seq
    return dnas

def searchWithDataFrames(dna_dict, sequence, sequence_length, euclidean_distance):
    neighbours = nonamere.buildNeighbourhood(sequence_length, sequence, euclidean_distance)["end_neighbour"].tolist()
    print("Euclidean:", euclidean_distance, "Shape: ", np.shape(neighbours))

def search(dna_dict, sequence, sequence_length, euclidean_distance):
    neighbours = nonamere.nonamere(sequence, euclidean_distance)
    print("euclidean: ", euclidean_distance, "shape: ", np.shape(neighbours))
    binding_list = []
    for neighbour in neighbours:
        for key in dna_dict.keys():
            start, tmp_occurences = 0, []
            while True:
                try:
                    tmp_occurences.append(dna_dict[key].index(neighbour[0], start))
                    s = tmp_occurences[-1]
                    binding = [key, s, s + sequence_length, neighbour[0], neighbour[1]]
                    binding_list.append(binding)
                    start = tmp_occurences[-1] + 1

                except:
                    break
    df_from_list = pd.DataFrame(binding_list, columns=["genome", "start", "stop", "sequence", "confidence"])
    return df_from_list
"""
def search(dna_dict, sequence):
    neighbours = nonamere.nonamere(sequence)

    for neighbour in neighbours:
        occurences = []


        #for secondKey in neighbour_dna_occ[key].keys():
         #   print(neighbour_dna_occ[key][secondKey])
"""

def main(sequence, file):
    sequence_length = len(sequence)
    print(sequence_length)
    dna_dict = makeDNADict(file)
    #print(nonamere.nonamere(sequence))
    occurences = {}
    df = search(dna_dict, sequence, sequence_length)
    print(df)
    df.to_csv("/homes/mschluet/PycharmProjects/Projektmodul/Bindungsstellen.csv")





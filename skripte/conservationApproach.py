import pandas as pd
import umapping
import translate
import nonamere as nona
import numpy as np

def search(sequence, conservation, j, dna_dict):
    conservation = conservation.tolist()
    neighbours = conservation_neighborhood(sequence, 5, conservation)
    print(type(neighbours))
    print("shape: ", np.shape(neighbours))
    binding_list = []
    for k in range(neighbours.shape[0]):
        for key in dna_dict.keys():
            start, tmp_occurences = 0, []
            while True:
                try:
                    tmp_occurences.append(dna_dict[key].index(neighbours["end_neighbour"].iloc[k], start))
                    s = tmp_occurences[-1]
                    binding = [key, s, neighbours["end_neighbour"].iloc[k], neighbours["distance"].iloc[k]]
                    binding_list.append(binding)
                    start = tmp_occurences[-1] + 1
                except:
                      break
    df_from_list = pd.DataFrame(binding_list, columns=["genome", "start", "sequence", "confidence"])
    df_from_list = df_from_list[df_from_list["genome"] == "4"]
    df_from_list.to_csv("/homes/mschluet/PycharmProjects/Projektmodul/CBF4_cons_matches" + str(j) + "_" + str(i) + ".csv")
    return df_from_list

def conservation_neighborhood(sequence, length, conservation):
    path_better = str(length) + "_mere_distances.csv"
    df_neighbour = pd.read_csv(path_better)


    tmp_neighbours = df_neighbour["Unnamed: 0"][df_neighbour[sequence[0:length]] <= conservation[0]]
    prefixe = tmp_neighbours.str.slice(stop=length - 1)
    suffixe = tmp_neighbours.str.slice(start=1)
    start = tmp_neighbours.str.slice(stop=1)
    stop = tmp_neighbours.str.slice(start=-1)
    tmp_neighbours = pd.concat(
        [tmp_neighbours.rename("neighbour"), prefixe.rename("prefix"), suffixe.rename("suffix"), start.rename("start"),
         stop.rename("stop")], axis=1)
    for i in range(len(sequence) - length):
        pentamere = sequence[i + 1:i + length +1]
        #print("pent: " +pentamere)
        #print(df_neighbour["Unnamed: 0"][df_neighbour[pentamere] <= conservation[i+1]])
        tmp_neighbours_next = df_neighbour["Unnamed: 0"][df_neighbour[pentamere] <= conservation[i+1]]
        tmp_neighbours_next.append(pd.Series(pentamere), ignore_index=True)
        prefixe = tmp_neighbours_next.str.slice(stop=length - 1)
        suffixe = tmp_neighbours_next.str.slice(start=1)
        start = tmp_neighbours_next.str.slice(stop=1)
        stop = tmp_neighbours_next.str.slice(start=-1)

        tmp_neighbours_next = pd.concat(
            [tmp_neighbours_next.rename("neighbour"), prefixe.rename("prefix"), suffixe.rename("suffix"),
             start.rename("start"), stop.rename("stop")], axis=1)
        tmp_neighbours = tmp_neighbours_next.merge(tmp_neighbours, left_on=['prefix'], right_on=['suffix'],
                                                   how='left').dropna()
        tmp_neighbours["start"] = tmp_neighbours["start_y"] + tmp_neighbours["start_x"]
        tmp_neighbours = tmp_neighbours.drop(axis=1, columns=["start_y", "start_x", "stop_y", "prefix_y", "suffix_y",
                                                              "neighbour_y"])
        tmp_neighbours = tmp_neighbours.rename(
            columns={"neighbour_x": "neighbour", "stop_x": "stop", "prefix_x": "prefix", "suffix_x": "suffix"})

        # tmp_neighbours.rename( )
    # print(tmp_neighbours)
    tmp_neighbours["end_neighbour"] = tmp_neighbours["start"] + tmp_neighbours["suffix"]
    reference = translate.seq_to_shape(sequence).dropna().to_numpy().flatten()
    confidence = tmp_neighbours["end_neighbour"].apply(nona.distance, args=(reference,))
    tmp_neighbours["distance"] = confidence
    tmp_neighbours = tmp_neighbours.drop(axis=1, columns=["neighbour", "prefix", "suffix", "start", "stop"])
    print(tmp_neighbours)
    return tmp_neighbours

if __name__ == '__main__':
    df = pd.read_csv("fimo_hits_chr4_forward.csv")
    fimo_hits = df
    df = pd.read_csv("/homes/mschluet/Dokumente/Projektmodul/average_motif_shape_cbf4.csv")
    average_df = df.dropna()
    average_df = df.drop([1, 2, 3, 4, 5, 6, 17, 18, 19, 20, 21])
    conservation = average_df["conservation"] + 1
    motif_length = average_df.shape[0]
    dna_dict = umapping.makeDNADict("TAIR10_chr_all.fas")
    chr4 = dna_dict["4"]
    max = fimo_hits["signal_value"].max()
    # euclidean_distance = sys.argv[3]
    for j in range(5):
        #print(max)
        #print(fimo_hits[fimo_hits["signal_value"] >= max]["start"])
        start = int(fimo_hits[fimo_hits["signal_value"] >= max]["start"].iloc[0])
        stop = int(fimo_hits[fimo_hits["signal_value"] >= max]["stop"].iloc[0])
        fimo_hits = fimo_hits.drop(fimo_hits[fimo_hits["signal_value"] >= max].index)
        max = fimo_hits["signal_value"].max()

        print(start)
        n = 2
        sequence = []
        for i in range(stop - start + (n * 2) + 1):
            # print(i)
            sequence.append(chr4[start - 1 - n + i])
        sequence = ''.join(sequence)
        print(sequence)
        df = search(sequence, conservation, j, dna_dict)



